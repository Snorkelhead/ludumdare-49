﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveWithSin : MonoBehaviour {

    //how much it goes up and down in time
    public float upDownFrequency;
    public float leftRightFrequency;
    //difference in upper and lower points
    public float sinMagnitudeUpDown;
    public float sinMagnitudeLeftRight;

    Vector3 position; 

	
	// Update is called once per frame
	void Update () {
        UpAndDown();
        LeftAndRight();
	}

    void UpAndDown()
    {
        position = transform.position;
        transform.position = position + transform.up * Mathf.Sin(Time.time * upDownFrequency) * sinMagnitudeUpDown;
    }

    void LeftAndRight()
    {
        position = transform.position;
        transform.position = position + transform.right * Mathf.Sin(Time.time * leftRightFrequency) * sinMagnitudeLeftRight;
    }

}
