﻿using UnityEngine;
using System.Collections;

public class InputManagerScript : MonoBehaviour {

	public int P1_xButton {get; set;}
    public int P2_xButton { get; set; }


    public float P1_verticalAxis {get; set;}
	public float P1_horizontalAxis {get; set;}
    public bool P1_keyHorizontalLeft;
    public bool P1_keyHorizontalRight;

    //public float P1_horz4thAxis {get; set;}
    //public float P1_vert5thAxis {get; set;}
    //public float P1_attackAxis {get; set;}
    //public float P1_rightTriggerAxis {get; set;}

    public float P2_verticalAxis { get; set; }
    public float P2_horizontalAxis { get; set; }
    public bool P2_keyHorizontalLeft;
    public bool P2_keyHorizontalRight;

    //public float P2_horz4thAxis { get; set; }
    //public float P2_vert5thAxis { get; set; }
    //public float P2_attackAxis { get; set; }
    //public float P2_rightTriggerAxis { get; set; }


    void Update () 
	{

        //P1 jump
		if(Input.GetButtonDown("00_Jump"))
		{
			P1_xButton = 1;
            //Debug.Log("1");
		}
		else if(Input.GetButton("00_Jump"))
		{
			P1_xButton = 2;
            //Debug.Log("2");
        }
		else if(Input.GetButtonUp("00_Jump"))
		{
			P1_xButton = 3;
            //Debug.Log("3");
        }
		else
		{
			P1_xButton = 0;
           // Debug.Log("0");
        }

        //P2 jump
        if (Input.GetButtonDown("01_Jump"))
        {
            P2_xButton = 1;
        }
        else if (Input.GetButton("01_Jump"))
        {
            P2_xButton = 2;
        }
        else if (Input.GetButtonUp("01_Jump"))
        {
            P2_xButton = 3;
        }
        else
        {
            P2_xButton = 0;
        }


        P1_verticalAxis = Input.GetAxis("00_Vertical");
		P1_horizontalAxis = Input.GetAxis("00_Horizontal");
        P1_keyHorizontalLeft = Input.GetButton("00_HorizontalKeyLeft");
        P1_keyHorizontalRight = Input.GetButton("00_HorizontalKeyRight");

        //P1_horz4thAxis  = Input.GetAxis("00_Horiz4th");
        //P1_vert5thAxis  = Input.GetAxis("00_Vert5th");
        //P1_attackAxis = Input.GetAxis("00_9th");
        //P1_rightTriggerAxis =  Input.GetAxis("00_10th");

        P2_verticalAxis = Input.GetAxis("01_Vertical");
        P2_horizontalAxis = Input.GetAxis("01_Horizontal");
        P2_keyHorizontalLeft = Input.GetButton("01_HorizontalKeyLeft");
        P2_keyHorizontalRight = Input.GetButton("01_HorizontalKeyRight");
        //P2_horz4thAxis = Input.GetAxis("01_Horiz4th");
        //P2_vert5thAxis = Input.GetAxis("01_Vert5th");
        //P2_attackAxis = Input.GetAxis("01_9th");
        //P2_rightTriggerAxis = Input.GetAxis("01_10th");
    }

}
