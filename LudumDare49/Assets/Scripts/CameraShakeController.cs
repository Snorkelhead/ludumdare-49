using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShakeController : MonoBehaviour
{
    float shakeTimeRemaining;
    float shakepower;
    float shakeFadeTime;

    float shakeRotation;
    [SerializeField] float rotationMultiplyer = 15;

    private void LateUpdate()
    {
        if (shakeTimeRemaining > 0)
        {
            shakeTimeRemaining -= Time.deltaTime;
            float xAmount = Random.Range(-.5f, .5f) * shakepower;
            float yAmount = Random.Range(-.5f, .5f) * shakepower;

            transform.position += new Vector3(xAmount, yAmount, 0);

            shakepower = Mathf.MoveTowards(shakepower, 0f, shakeFadeTime * Time.deltaTime);

            shakeRotation = Mathf.MoveTowards(shakeRotation, 0f, shakeFadeTime * rotationMultiplyer * Time.deltaTime);
        }

        transform.rotation = Quaternion.Euler(0, 0, shakeRotation * Random.Range(-.5f, .5f));

    }

    public void StartShake(float length, float power)
    {
        shakeTimeRemaining = length;
        shakepower = power;

        shakeFadeTime = power / length;

        shakeRotation = power * rotationMultiplyer;
    }

}
