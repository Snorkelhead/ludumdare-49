using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Energy : MonoBehaviour
{
    public GameObject child;
    public AudioClip appleBite;
    public AudioSource m_audioSource;

    [SerializeField] public int energy;

    private void Awake()
    {
        energy = 0;
        m_audioSource = GetComponentInParent<AudioSource>();
        
    }

    void AppleUp(int appleEnergy)
    {
        m_audioSource.clip = appleBite;
        m_audioSource.Play();
        energy += appleEnergy;
    }

    private void Update()
    {
        if (energy == 10)
        {
            DestroyObject(child);
        }
    }

}
