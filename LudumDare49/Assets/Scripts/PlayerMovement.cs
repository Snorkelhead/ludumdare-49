using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    //Movement Variables
    public CharacterController controller;
    [SerializeField] bool isRunning;
    private float speed;
    private float acc;
    private float decel;
    [SerializeField] float accTime;
    [SerializeField] float decelTime;
    [SerializeField] float maxSpeed;

    //Sprite Variables
    SpriteRenderer m_Renderer;
    Animator m_Animator;

    private void Awake()
    {
        //component reference
        controller = GetComponent<CharacterController>();
        m_Renderer = GetComponentInChildren<SpriteRenderer>();
        m_Animator = GetComponent<Animator>();
    }

    private void Start()
    {
        //set accel + deccel
        acc = maxSpeed / accTime;
        decel = maxSpeed / decelTime;
    }

    // Update is called once per frame
    void Update()
    {
        GetMovementInput();
        GetPlayerDirection();
    }

    void GetMovementInput()
    {
        //Get input
        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

        //apply accel and deccel
        if (z != 0 || x != 0)
        {
            isRunning = true;
            m_Animator.SetFloat("Speed", 1);
            speed = Mathf.Min(speed + acc * Time.deltaTime, maxSpeed);
        }
        else
        {
            m_Animator.SetFloat("Speed", 0);
            isRunning = false;
            speed = Mathf.Max(speed - decel * Time.deltaTime, 0);
        }

        //clamp and apply movement 
        Vector3 clampedMotion = Vector3.ClampMagnitude(transform.right * x + transform.up * z, 1) * speed;
        controller.Move(clampedMotion * Time.deltaTime);
    }

    void GetPlayerDirection()
    {
        float moveHorizontal = Input.GetAxisRaw ("Horizontal");
        float moveVertical = Input.GetAxisRaw ("Vertical");
        Vector2 movement = new Vector2(moveHorizontal, moveVertical);

        //Right Vector2 (1.0f, 0.0f)
        //Left Vector2 (-1.0f, 0.0f)
        //Up  Vector2 (0.0f, 1.0f)
        //Down  Vector2 (0.0f, -1.0f)

        if ( Mathf.Abs(movement.x + movement.y) > 1.0)
        {
            if(movement.x == 1.0f)
            {
                //Moving Right
                m_Renderer.flipX = false;
                m_Animator.SetBool("Horz", true);
                m_Animator.SetBool("Up", false);
                m_Animator.SetBool("Down", false);
            }
            if(movement.x == -1.0f)
            {
                //Moving Left
                m_Renderer.flipX = true;
                m_Animator.SetBool("Horz", true);
                m_Animator.SetBool("Up", false);
                m_Animator.SetBool("Down", false);
            }
        }
        else
        {
            if(movement.x == 1.0f)
            {
                //Moving Right
                m_Renderer.flipX = false;
                m_Animator.SetBool("Horz", true);
                m_Animator.SetBool("Up", false);
                m_Animator.SetBool("Down", false);
            }
            else if(movement.x == -1.0f)
            {
                //Moving Left
                m_Renderer.flipX = true;
                m_Animator.SetBool("Horz", true);
                m_Animator.SetBool("Up", false);
                m_Animator.SetBool("Down", false);
            }
            else if(movement.y == 1.0f)
            {
                //Moving Up
                m_Renderer.flipX = false;
                m_Animator.SetBool("Horz", false);
                m_Animator.SetBool("Up", true);
                m_Animator.SetBool("Down", false);
            }
            else if(movement.y == -1.0f)
            {
                // Moving Down
                m_Renderer.flipX = false;
                m_Animator.SetBool("Horz", false);
                m_Animator.SetBool("Up", false);
                m_Animator.SetBool("Down", true);            
            }            
        }

    }

}
