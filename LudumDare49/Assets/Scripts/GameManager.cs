﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public bool testLevel = false;
    private AudioManager am;

    void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
        am = FindObjectOfType<AudioManager>();
    }


    void Update()
    {
        /*
        if (Input.GetKey("r"))
        {
            ReloadGame();
        }
        */

        if ( Input.GetKeyDown( KeyCode.Escape) )
        {
           Application.Quit();
        }
        // This needs to go or it will reload the level every space press
        if ( Input.GetKeyDown( KeyCode.Space) )
        {
           SceneManager.LoadScene("MainLevel", LoadSceneMode.Single);
            am.UpdateMusicContext("Happy");
        }

    }


    public void ReloadGame()
    {
        SceneManager.LoadScene("BOOT_Scene", LoadSceneMode.Single);
        am.UpdateMusicContext("Happy");
        am.audioHeartBeat.volume = 0.0f;
        Destroy(am.gameObject);
        Destroy(this.gameObject);


    }
}
