
using UnityEngine;

public class DelayCamera : MonoBehaviour
{

    [SerializeField] Transform target;
    [SerializeField] float smoothSpeed;
    [SerializeField] Vector3 cameraOffset;

    private void Update()
    {
        Vector3 desirePosition = target.position + cameraOffset;
        Vector3 smoothPosition = Vector3.Lerp(transform.position, desirePosition, smoothSpeed * Time.deltaTime);
        transform.position = smoothPosition;

      // transform.LookAt(target);
    }


}
