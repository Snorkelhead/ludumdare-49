using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Apple : MonoBehaviour
{
    private HealthandEnergy healthandEnergy;
    private int AppleEnergy = 1;
    private void Awake()
    {
        healthandEnergy = GameObject.FindGameObjectWithTag("HealthManager").GetComponent<HealthandEnergy>();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            collision.gameObject.SendMessage("AppleUp", AppleEnergy);
            healthandEnergy.UpdatePlayerStats();
            Destroy(this.gameObject);
        }
    }
}
