using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStability : MonoBehaviour
{
    public bool isUnstable = false;
    public Transform stableLocation;
    public float distanceFromStable;
    private AudioManager am;

    void Awake()
    {
        am = GameObject.FindGameObjectWithTag("AudioManager").GetComponent<AudioManager>();
    }

    void Update ()
    {
        CalculateStableDistance();
        float heartbeatVolume = (1.0f/50.0f) * distanceFromStable;
        am.audioHeartBeat.volume = Mathf.Clamp( heartbeatVolume, 0.0f, 1.0f ) ;
    }

    public void isPlayerStable(bool state)
    {
        isUnstable = state;
    }

    void CalculateStableDistance()
    {
        distanceFromStable = Vector2.Distance(this.transform.position, stableLocation.position);
    }



    void OnTriggerEnter2D(Collider2D col)
    {
        ContextTrigger contextTrigger = col.gameObject.GetComponent<ContextTrigger>();

        if(col.gameObject.tag == "ContextTrigger")
        {
            if(contextTrigger.isUnstable == true && isUnstable == false )
            {
                isPlayerStable(true);
                am.UpdateMusicContext("Fight");
            }
            if(contextTrigger.isUnstable == false  && isUnstable == true )
            {
                isPlayerStable(false);
                am.UpdateMusicContext("Happy");
            }
        
        }

    }
}
