using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class HealthandEnergy : MonoBehaviour
{

    public Image healthFill;
    public Image energyFill;
    public Health playerHealth;
    public Energy playerEnergy;
    private float startHealth = 10f;
    private float startEnergy = 0f;
    // Start is called before the first frame update
    void Start()
    {
        healthFill.fillAmount = startHealth;
        energyFill.fillAmount = startEnergy;
    }
    public void UpdatePlayerStats()
    {
        int currentPlayerHealth = playerHealth.health;
        int currentPlayerEnergy = playerEnergy.energy;
        healthFill.fillAmount = (float)currentPlayerHealth/10;
        energyFill.fillAmount = (float)currentPlayerEnergy/10;
    }
}
