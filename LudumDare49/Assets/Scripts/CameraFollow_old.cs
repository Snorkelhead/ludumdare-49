﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{

    public Transform player1;
    public Transform player2;

    private Vector2 player1Pos;
    private Vector2 player2Pos;

    void Awake()
    {
        
    }

    void Update()
    {
        player1Pos = player1.position;
        player2Pos = player2.position;

        Vector3 newPosition = player1Pos + (player2Pos - player1Pos) / 2;


        transform.position = new Vector3(newPosition.x, 0.0f, transform.position.z);
    }
}
