using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public AudioSource audioMusic;
    public AudioSource audioHeartBeat;
    public AudioSource audioSFX;
    public AudioClip happyMusic;
    public AudioClip fightMusic; 
    public AudioClip deathMusic;
    public AudioClip winMusic; 
    private string MusicContext = "Happy"; 

    public List<AudioClip> sfxList = new List<AudioClip>();



    void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
    }

    void Start()
    {
        audioMusic.Play(0);
        Debug.Log("started");
    }

    void Update()
    {
        //Testing for Heartbeat
        /*
        if(Input.GetKeyDown("h"))
        {
            if(audioHeartBeat.isPlaying == true)
            {
                Debug.Log("pause it");
                audioHeartBeat.Pause();
            }
            else
            {   
                Debug.Log("play it");
                audioHeartBeat.Play(0);
            }
        }
        */
        if(Input.GetKeyDown("h"))
        {
            UpdateMusicContext("Win");
        }   
        if(Input.GetKeyDown("j"))
        {
            UpdateMusicContext("Death");
        }         

    }

    public void UpdateMusicContext(string context)
    {
        MusicContext = context;
        if(MusicContext == "Happy")
        {
            audioMusic.clip = happyMusic;
            audioMusic.Play(0);
        }
        else if(MusicContext == "Fight")
        {
            audioMusic.clip = fightMusic;
            audioMusic.Play(0);
        }
        else if(MusicContext == "Death")
        {
            audioMusic.clip = deathMusic;
            audioMusic.Play(0);
        }
        else if(MusicContext == "Win")
        {
            audioMusic.clip = winMusic;
            audioMusic.Play(0);
        }
        else
        {
            audioMusic.clip = happyMusic;
            audioMusic.Play(0);           
        }
    }

    public void HorseYell()
    {
        Debug.Log("A horse is in pain");
        int randPick = Random.Range(0, 2);
        audioSFX.clip = sfxList[randPick];
        audioSFX.Play(0);
    }
}
