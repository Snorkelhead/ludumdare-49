using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class Turret : MonoBehaviour
{
   
    // horse
    private Transform horse;

    // tracking
    public Transform Target;

    // Fire stuff
    
    public float firecooldown = 2f;
    public float fireRate = 2f;
    private float fireTimer = 0f;
    public AudioClip gunSound;
    AudioSource m_audioSource;

    //bullet stuffs
    public float Speed = 1f;
    public GameObject bulletPrefab;
    public Transform firePoint;
    private float bulletTimer = 0f;
    public float bulletCooldown = 1f;
    int bulletCount = 0;

    private void Awake()
    {
        m_audioSource = GetComponent<AudioSource>();
        m_audioSource.clip = gunSound;
    }

    // Update is called once per frame
    void Update()
    {
       
        fireTimer += Time.deltaTime;
        if (fireTimer >= firecooldown)
        {
            bulletTimer += Time.deltaTime;
            if (bulletCount < fireRate)
            {
                if (bulletTimer >= bulletCooldown)
                {
                    Shoot();
                    

                    bulletCount += 1;
                    bulletTimer = 0f;
                }
                
                
            }
            else
            {
                fireTimer = 0f;
                bulletCount = 0;
            }
        }
    }

    private void Shoot()
    {
        m_audioSource.Play(0);
        Vector2 direction = Target.position - firePoint.position;
        Quaternion bulletDirection = Quaternion.LookRotation(direction)* Quaternion.FromToRotation(Vector3.right, Vector3.forward);
        GameObject newBullet;
        newBullet = Instantiate(bulletPrefab, firePoint.position, bulletDirection) as GameObject;
        newBullet.GetComponent<Rigidbody2D>().AddForce(direction * Speed, ForceMode2D.Impulse);
    }
    void HitTarget()
    {
        

    }
}
