using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class Win: MonoBehaviour
{
    private AudioManager am;
   
    private void Awake()
    {
        am = FindObjectOfType<AudioManager>();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            am.UpdateMusicContext("Win");
            Destroy(this.gameObject);
            SceneManager.LoadScene("winScene", LoadSceneMode.Single);
        }
    }
}
