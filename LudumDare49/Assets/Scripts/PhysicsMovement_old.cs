﻿using UnityEngine;
using System.Collections;

public class PhysicsMovement : MonoBehaviour
{
    public int playerNumber;
    public bool hasWon;
    private GameManager gameManager;

    public int jump { get; set; }
    public int jumpCount { get; set; }
    public bool tJump { get; set; }
    private bool inAir;
    public bool isMoving { get; set; }
    public bool isMovingRight;
    public bool isMovingLeft;

    public bool land { get; set; }
    public bool grounded;
    public bool landLeft;
    public  bool landRight;

    public bool controlFacing { get; set; }
    public bool facingLeft { get; set; }

    public float jumpPower;
    public float heldJumpPowerModifier;
    public float heldJumpPower;
    private float startHeldJumpPower;
    public float movePower;
    public float inAirMoveModifier;

    private float moveDeadZone = 0.1f;

    private float vertAxis;
    private float horzAxis;

    private bool keyHorizLeft;
    private bool keyHorizRight;

    public Transform LandDetectObjectLeft;
    public Transform LandDetectObjectRight;

    private Rigidbody2D playerBody;
    public InputManagerScript inputManager;

    private Rigidbody2D rb;

    public bool isHoldingJump;

    void Start()
    {
        gameManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();

        playerBody = gameObject.GetComponent<Rigidbody2D>();
        rb = gameObject.GetComponent<Rigidbody2D>();

        if (playerNumber == 1)
        {
            //heldJumpPower = jumpPower;
            startHeldJumpPower = heldJumpPower;
        }
        if (playerNumber == 2)
        {
            //heldJumpPower = jumpPower;
            startHeldJumpPower = heldJumpPower;
        }
        controlFacing = true;
        facingLeft = true;
    }

    private void FixedUpdate()
    {
        float deltaTime = Time.fixedDeltaTime;

        // casting down for ground:
        int combinedMask;
        int mask1;
        int mask2;
        if (playerNumber == 1)
        {
            mask1 = 1 << LayerMask.NameToLayer("Ground");
            mask2 = 1 << LayerMask.NameToLayer("P1_Ground");
            combinedMask = mask1 | mask2;
            
            landLeft = Physics2D.CircleCast(transform.position, 0.2f, Vector2.down, 0.25f, combinedMask);
            //landRight = Physics2D.Linecast(transform.position, LandDetectObjectRight.position, combinedMask);
        }
        else
        {
            mask1 = 1 << LayerMask.NameToLayer("Ground");
            mask2 = 1 << LayerMask.NameToLayer("P2_Ground");
            combinedMask = mask1 | mask2;

            landLeft = Physics2D.Linecast(transform.position, LandDetectObjectRight.position, combinedMask);
            landRight = Physics2D.Linecast(transform.position, LandDetectObjectRight.position, combinedMask);
        }

        if (isHoldingJump == true)
        {
            if (playerNumber == 1)
            {
                playerBody.AddForce(new Vector2(0.0f, heldJumpPower), ForceMode2D.Force);
            }
            if (playerNumber == 2)
            {
                playerBody.AddForce(new Vector2(0.0f, -heldJumpPower), ForceMode2D.Force);
            }
        }
        if (isMovingRight == true)
        {
            if (grounded == true)
            {
                playerBody.AddForce(new Vector2(movePower * deltaTime, 0.0f), ForceMode2D.Force);
            }
            else
            {
                playerBody.AddForce(new Vector2((movePower * deltaTime) * inAirMoveModifier, 0.0f), ForceMode2D.Force);
            }
        }
        if (isMovingLeft == true)
        { 
            if (grounded == true)
            {
                playerBody.AddForce(new Vector2(-movePower * deltaTime, 0.0f), ForceMode2D.Force);
            }
            else
            {
                playerBody.AddForce(new Vector2((-movePower * deltaTime) * inAirMoveModifier, 0.0f), ForceMode2D.Force);
            }
        }

    }


    void Update()
    {

        // Getting delta time and input:
        float deltaTime = Time.deltaTime;
        if (playerNumber == 1)
        {
            jump = inputManager.P1_xButton;
            vertAxis = inputManager.P1_verticalAxis;
            horzAxis = inputManager.P1_horizontalAxis;
            keyHorizLeft = inputManager.P1_keyHorizontalLeft;
            keyHorizRight = inputManager.P1_keyHorizontalRight;
        }
        else
        {
            jump = inputManager.P2_xButton;
            vertAxis = inputManager.P2_verticalAxis;
            horzAxis = inputManager.P2_horizontalAxis;
            keyHorizLeft = inputManager.P2_keyHorizontalLeft;
            keyHorizRight = inputManager.P2_keyHorizontalRight;
        }
        Debug.DrawLine(transform.position, LandDetectObjectRight.position);
        Debug.DrawLine(transform.position, LandDetectObjectLeft.position);

        // start jump Block //
        if (jump == 1)
        {
            jumpCount += 1;
            if (jumpCount <= 2)
            {
                if (controlFacing == true || jumpCount == 2)
                {
                    playerBody.velocity = new Vector2(playerBody.velocity.x, 0.0f);
                    if (playerNumber == 1)
                    {
                        playerBody.AddForce(new Vector2(0.0f, jumpPower), ForceMode2D.Impulse);
                    }
                    if (playerNumber == 2)
                    {
                        playerBody.AddForce(new Vector2(0.0f, -jumpPower), ForceMode2D.Impulse);
                    }
                }
            }

            inAir = true;
            grounded = false;
        }
        else if (jump == 2 && jumpCount <= 2)
        {
            heldJumpPower -= heldJumpPower * heldJumpPowerModifier * deltaTime;
            if (heldJumpPower <= 1.0f)
            {
                heldJumpPower = 0.0f;
            }
            //Debug.Log(playerNumber);

            //Debug.Log (heldJumpPower);
            isHoldingJump = true;


        }
        else if (jump == 3)
        {
            if (playerNumber == 1)
            {
                heldJumpPower = startHeldJumpPower;
                isHoldingJump = false;
            }
            if (playerNumber == 2)
            {
                heldJumpPower = startHeldJumpPower;
                isHoldingJump = false;
            }
        }
        // end jump Block //

        // start if controlling facing block //
        if (controlFacing == true)
        {
            // start moving block //
            if (horzAxis >= moveDeadZone || keyHorizLeft == true)
            {

                if (controlFacing == true)
                {
                    ScaleTurnSprite(-1.0f);
                    facingLeft = false;
                }
                isMoving = true;
                isMovingLeft = false;
                isMovingRight = true;
            }
            else if (horzAxis <= -moveDeadZone || keyHorizRight == true)
            {

                if (controlFacing == true)
                {
                    ScaleTurnSprite(1.0f);
                    facingLeft = true;
                }
                isMoving = true;
                isMovingLeft = true;
                isMovingRight = false;
            }
            else
            {
                isMoving = false;
                isMovingLeft = false;
                isMovingRight = false;
            }
            // end moving block //


            //Debug.Log (land);
            // Check for ground to play land
            if ((landLeft || landRight) && rb.velocity.y <= 0.0f)
            {

                if (grounded == false)
                {
                    land = true;
                }
                else
                {
                    land = false;
                }
                jumpCount = 0;
                grounded = true;

            }
            else
            {
                land = false;
                grounded = false;
            }


            if (landLeft == false && landRight == false && inAir == false)
            {
                tJump = true;
                inAir = true;
            }
            else
            {
                tJump = false;
            }
            if (grounded == true)
            {
                inAir = false;
            }
        }
        else
        {
            // while not controlling else //
            if ((landLeft || landRight) && rb.velocity.y <= 0.0f)
            {
                jumpCount = 0;
                grounded = true;
            }

            if (landLeft == false && landRight == false)
            {
                grounded = false;

            }
            inAir = false;

        }
        // end if controlling facing block //

    }

    public void ScaleTurnSprite(float facing)
    {
        Vector3 currentScale = transform.localScale;

        transform.localScale = new Vector3(facing, currentScale.y, currentScale.z);
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if(hasWon == false && col.tag == "WinZone")
        {
            hasWon = true;
        }
    }
}
