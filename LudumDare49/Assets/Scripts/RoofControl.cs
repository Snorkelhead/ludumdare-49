using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoofControl : MonoBehaviour
{

    [SerializeField] bool canSee;
    SpriteRenderer m_renderer;

    private void Awake()
    {
        m_renderer = GetComponent<SpriteRenderer>();
        canSee = false;
    }

    private void Start()
    {
        m_renderer.color = new Color(100, 100, 100, 0);
    }

    private void Update()
    {
      
    }

    void RoofColour()
    {
        if (canSee)
        {
            m_renderer.color = new Color(100, 100, 100, 100);
        }
        else if (!canSee)
        {
            m_renderer.color = new Color(100, 100, 100, 0);
        }
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            if (canSee)
            {
                canSee = false;
                RoofColour();
            } else
            {
                canSee = true;
                RoofColour();
            }
        }
    }

}
