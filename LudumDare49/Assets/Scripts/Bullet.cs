using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class Bullet : MonoBehaviour
{
    private HealthandEnergy healthandEnergy;
    public float lifeSpan = 0f;
    public float killPoint = 1f;

    public GameObject bloodPrefab;

    Camera MainCamera;
  
  
    // Start is called before the first frame update
    void Start()
    {
        MainCamera = Camera.main;
        healthandEnergy = GameObject.FindGameObjectWithTag("HealthManager").GetComponent<HealthandEnergy>();
    }

    // Update is called once per frame
    void Update()
    {
        lifeSpan += Time.deltaTime;
        if (lifeSpan >= killPoint)
        {
            Destroy(this.gameObject);
            lifeSpan = 0f;
        }
    }
    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            MainCamera.GetComponent<CameraShakeController>().StartShake(.5f, .5f);
            other.gameObject.GetComponent<Health>().health -= 1;
            healthandEnergy.UpdatePlayerStats();
            Destroy(this.gameObject);
            lifeSpan = 0f;
            other.gameObject.GetComponent<Health>().DoYell();
            Instantiate(bloodPrefab, transform.position, Quaternion.identity);
        }
        
    }
}
