using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(AudioSource))]
public class Health : MonoBehaviour
{
    public int health = 100;
    private int currHealth;
    private GameManager gm;
    private AudioManager am;
    public PlayerMovement playerMovement;
    public Animator m_Animator;
    Camera MainCamera;
    public GameObject turretPrefab;

    public GameObject[] bullets;

    AudioSource m_audioSource;
    public AudioClip spaltter;

    // Start is called before the first frame update
    void Awake()
    {
        MainCamera = Camera.main;
        am = FindObjectOfType<AudioManager>();
        playerMovement = GetComponentInParent<PlayerMovement>();
        m_Animator = GetComponentInParent<Animator>();
        m_audioSource = GetComponent<AudioSource>();
        m_audioSource.clip = spaltter;

    }


    // Update is called once per frame
    void Update()
    {
        if (health <= 0)
        {
            StartCoroutine(Death());
        }

    }

    public void DoYell()
    {
        am.HorseYell();
    }

    void TurnOffBullets()
    {
        DestroyImmediate(turretPrefab, true);
        bullets = GameObject.FindGameObjectsWithTag("Bullet");

        for (int i = bullets.Length - 1; i >= 0; i--)
        {
            Destroy(bullets[i].gameObject);
        }

    }


    IEnumerator Death()
    {
        TurnOffBullets();
        playerMovement.enabled = false;
        am.UpdateMusicContext("Death");

        yield return new WaitForSeconds(1);

        if (!m_audioSource.isPlaying)
        {
            m_audioSource.PlayOneShot(spaltter);
        }
        m_Animator.SetTrigger("Dead");
        MainCamera.GetComponent<CameraShakeController>().StartShake(.5f, .5f);

        yield return new WaitForSeconds(2);

        gm = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        SceneManager.LoadScene("Death_Scene", LoadSceneMode.Single);
    }

}
